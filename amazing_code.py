INPUT_BRANCHES_LIST = [
        {
            "brand_id": 20,
            "distance": 30,
            "is_open": True
        },
        {
            "brand_id": 20,
            "distance": 45,
            "is_open": True
        },
        {
            "brand_id": 21,
            "distance": 2,
            "is_open": False
        },
        {
            "brand_id": 320,
            "distance": 60,
            "is_open": True
        },
        {
            "brand_id": 54,
            "distance": 30,
            "is_open": True
        },
        {
            "brand_id": 20,
            "distance": 15,
            "is_open": False
        }
    ]

def sort_by_distance(branches_list):
    branches_list.sort(key=lambda branch: branch['distance'])
    return branches_list

def filter_unique_branches(branches_list):
    branches = set()

    new_list = []

    for branch in branches_list:
        brand_id = branch['brand_id']
        if brand_id not in branches:
            new_list.append(branch)
            branches.add(brand_id)
            

    return new_list

def filter_open_branches(branches_list):
    return [b for b in branches_list if b["is_open"]]

def filter_and_sort_branch_list(branches_list = []):
    print("<INSERT YOUR NAME HERE>")

    branches_list = sort_by_distance(branches_list)
    branches_list = filter_unique_branches(branches_list)
    branches_list = filter_open_branches(branches_list)

    print("FINAL BRANCH LIST:", branches_list)

if __name__ == "__main__":
    filter_and_sort_branch_list(INPUT_BRANCHES_LIST)